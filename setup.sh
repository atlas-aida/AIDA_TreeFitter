export AIDATF=$PWD
if [ -z "$HISTFITTER" ]; then
    cd $HOME/Software/HistFitter
    source setup.sh
fi
export PYTHONPATH=$HOME/Software/python:$PYTHONPATH
export PYTHONPATH=$PWD:$PYTHONPATH
cd $AIDATF
