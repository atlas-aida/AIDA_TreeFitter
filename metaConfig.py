import anakit.ana as aka

analysisName = 'aidaFitProto'

files_dataPrefix = '/Users/ddavis/Software/LocalData/nomap29nv/data'
files_mcPrefix   = '/Users/ddavis/Software/LocalData/nomap29nv/mc'
fileLists = {
    'Data'    : [files_dataPrefix+'/data.root'] ,
    'ttbar'   : [files_mcPrefix+'/'+str(dsid)+'.root' for dsid in aka.get_dsids('ttbar_PowPy8')] ,
    'Wt'      : [files_mcPrefix+'/'+str(dsid)+'.root' for dsid in aka.get_dsids('Wt_PowPy6')] ,
    'WW'      : [files_mcPrefix+'/'+str(dsid)+'.root' for dsid in aka.get_dsids('WW_PowPy8')] ,
    'Zjets'   : [files_mcPrefix+'/'+str(dsid)+'.root' for dsid in aka.get_dsids('Zjets_S221')] ,
    'Wjets'   : [files_mcPrefix+'/'+str(dsid)+'.root' for dsid in aka.get_dsids('Wjets_S221')] ,
    'Diboson' : [files_mcPrefix+'/'+str(dsid)+'.root' for dsid in aka.get_dsids('Diboson_PowPy8') ] ,
    'Fakes'   : []
}

for flk,flv in fileLists.iteritems():
    if flk != 'Data' and flk != 'Fakes':
        fileLists['Fakes'] += flv

if __name__ == '__main__':
    for fl, l in fileLists.iteritems():
        for xx in l:
            print xx
