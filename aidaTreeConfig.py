###########################################################################
###  adapted by D. Davis from wtTreeConfig.py, from K. Kinelli 2016-17  ###
###  contact: ddavis@cern.ch                                            ###
###########################################################################

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue
from ROOT import kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from ROOT import TCanvas, TLegend
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic

import metaConfig
analysisName = metaConfig.analysisName
fileLists    = metaConfig.fileLists

import logger
log = logger.Logger(analysisName+"Example")
log.setLevel(logger.INFO)


"""
Dictionary for our samples
key: Sample name
val: [ signal(1) or bkg(0), color ]
"""
mySamples = {
    "Wt"      : [ 0 , kAzure+2  ] ,
    "WW"      : [ 1 , kGreen+2  ] ,
    "Zjets"   : [ 1 , kOrange+2 ] ,
    "ttbar"   : [ 1 , kWhite    ] ,
    "Diboson" : [ 0 , kGray     ] ,
    "Fakes"   : [ 0 , kBlack    ]
}

"""
Dictionary for systematics with seperate trees
key: systematic name
val: [up, down, normonly (stolen from Wt config, need to look into further)]
"""
systKeyListTreesSym = {
    'EG_RESOLUTION_ALL'                           : ['_EG_RESOLUTION_ALL__1up'                          , '_EG_RESOLUTION_ALL__1down'                           , True  ] ,
    'EG_SCALE_ALL'                                : ['_EG_SCALE_ALL__1up'                               , '_EG_SCALE_ALL__1down'                                , True  ] ,
    'MUON_ID'                                     : ['_MUON_ID__1up'                                    , '_MUON_ID__1down'                                     , False ] ,
    'MUON_MS'                                     : ['_MUON_MS__1up'                                    , '_MUON_MS__1down'                                     , False ] ,
    'MUON_SCALE'                                  : ['_MUON_SCALE__1up'                                 , '_MUON_SCALE__1down'                                  , False ] ,
    'MUON_SAGITTA_RESBIAS'                        : ['_MUON_SAGITTA_RESBIAS__1up'                       , '_MUON_SAGITTA_RESBIAS__1down'                        , False ] ,
    'MUON_SAGITTA_RHO'                            : ['_MUON_SAGITTA_RHO__1up'                           , '_MUON_SAGITTA_RHO__1down'                            , False ] ,
    'MET_SoftTrk_Scale'                           : ['_MET_SoftTrk_ScaleUp'                             , '_MET_SoftTrk_ScaleDown'                              , False ] ,
    'JET_21NP_JET_BJES_Response'                  : ['_JET_21NP_JET_BJES_Response__1up'                 , '_JET_21NP_JET_BJES_Response__1down'                  , False ] ,
    'JET_21NP_JET_EffectiveNP_1'                  : ['_JET_21NP_JET_EffectiveNP_1__1up'                 , '_JET_21NP_JET_EffectiveNP_1__1down'                  , False ] ,
    'JET_21NP_JET_EffectiveNP_2'                  : ['_JET_21NP_JET_EffectiveNP_2__1up'                 , '_JET_21NP_JET_EffectiveNP_2__1down'                  , True  ] ,
    'JET_21NP_JET_EffectiveNP_3'                  : ['_JET_21NP_JET_EffectiveNP_3__1up'                 , '_JET_21NP_JET_EffectiveNP_3__1down'                  , True  ] ,
    'JET_21NP_JET_EffectiveNP_4'                  : ['_JET_21NP_JET_EffectiveNP_4__1up'                 , '_JET_21NP_JET_EffectiveNP_4__1down'                  , True  ] ,
    'JET_21NP_JET_EffectiveNP_5'                  : ['_JET_21NP_JET_EffectiveNP_5__1up'                 , '_JET_21NP_JET_EffectiveNP_5__1down'                  , True  ] ,
    'JET_21NP_JET_EffectiveNP_6'                  : ['_JET_21NP_JET_EffectiveNP_6__1up'                 , '_JET_21NP_JET_EffectiveNP_6__1down'                  , True  ] ,
    'JET_21NP_JET_EffectiveNP_7'                  : ['_JET_21NP_JET_EffectiveNP_7__1up'                 , '_JET_21NP_JET_EffectiveNP_7__1down'                  , True  ] ,
    'JET_21NP_JET_EffectiveNP_8restTerm'          : ['_JET_21NP_JET_EffectiveNP_8restTerm__1up'         , '_JET_21NP_JET_EffectiveNP_8restTerm__1down'          , True  ] ,
    'JET_21NP_JET_EtaIntercalibration_Modelling'  : ['_JET_21NP_JET_EtaIntercalibration_Modelling__1up' , '_JET_21NP_JET_EtaIntercalibration_Modelling__1down'  , False ] ,
    'JET_21NP_JET_EtaIntercalibration_NonClosure' : ['_JET_21NP_JET_EtaIntercalibration_NonClosure__1up', '_JET_21NP_JET_EtaIntercalibration_NonClosure__1down' , True  ] ,
    'JET_21NP_JET_EtaIntercalibration_TotalStat'  : ['_JET_21NP_JET_EtaIntercalibration_TotalStat__1up' , '_JET_21NP_JET_EtaIntercalibration_TotalStat__1down'  , False ] ,
    'JET_21NP_JET_Flavor_Composition'             : ['_JET_21NP_JET_Flavor_Composition__1up'            , '_JET_21NP_JET_Flavor_Composition__1down'             , False ] ,
    'JET_21NP_JET_Flavor_Response'                : ['_JET_21NP_JET_Flavor_Response__1up'               , '_JET_21NP_JET_Flavor_Response__1down'                , False ] ,
    'JET_21NP_JET_Pileup_OffsetMu'                : ['_JET_21NP_JET_Pileup_OffsetMu__1up'               , '_JET_21NP_JET_Pileup_OffsetMu__1down'                , False ] ,
    'JET_21NP_JET_Pileup_OffsetNPV'               : ['_JET_21NP_JET_Pileup_OffsetNPV__1up'              , '_JET_21NP_JET_Pileup_OffsetNPV__1down'               , True  ] ,
    'JET_21NP_JET_Pileup_PtTerm'                  : ['_JET_21NP_JET_Pileup_PtTerm__1up'                 , '_JET_21NP_JET_Pileup_PtTerm__1down'                  , True  ] ,
    'JET_21NP_JET_Pileup_RhoTopology'             : ['_JET_21NP_JET_Pileup_RhoTopology__1up'            , '_JET_21NP_JET_Pileup_RhoTopology__1down'             , True  ] ,
    'JET_21NP_JET_PunchThrough_MC15'              : ['_JET_21NP_JET_PunchThrough_MC15__1up'             , '_JET_21NP_JET_PunchThrough_MC15__1down'              , True  ] ,
    #'JET_21NP_JET_SingleParticle_HighPt'          : ['_JET_21NP_JET_SingleParticle_HighPt__1up'         , '_JET_21NP_JET_SingleParticle_HighPt__1down'          , True  ]
}

systKeyListTreesAsym = [
    ['MET_SoftTrk_ResoPara'   , '_MET_SoftTrk_ResoPara'  ] ,
    ['MET_SoftTrk_ResoPerp'   , '_MET_SoftTrk_ResoPerp'  ] ,
    ['JET_JER_SINGLE_NP__1up' , '_JET_JER_SINGLE_NP__1up']
]


systKeyListWeightSysNorm = {
    #"bTagSF_77_extrapolation_from_charm" : [ "weightSyswLum_bTagSF_77_extrapolation_from_charm_up" , "weightSyswLum_bTagSF_77_extrapolation_from_charm_down" ] ,
    #"bTagSF_77_extrapolation"            : [ "weightSyswLum_bTagSF_77_extrapolation_up"            , "weightSyswLum_bTagSF_77_extrapolation_down"            ] ,
    "jvt"                                : [ "weightSyswLum_jvt_UP"                                , "weightSyswLum_jvt_DOWN"                                ] ,
    "leptonSF_EL_SF_ID"                  : [ "weightSyswLum_leptonSF_EL_SF_ID_UP"                  , "weightSyswLum_leptonSF_EL_SF_ID_DOWN"                  ] ,
    "leptonSF_EL_SF_Isol"                : [ "weightSyswLum_leptonSF_EL_SF_Isol_UP"                , "weightSyswLum_leptonSF_EL_SF_Isol_DOWN"                ] ,
    "leptonSF_EL_SF_Reco"                : [ "weightSyswLum_leptonSF_EL_SF_Reco_UP"                , "weightSyswLum_leptonSF_EL_SF_Reco_DOWN"                ] ,
    "leptonSF_EL_SF_Trigger"             : [ "weightSyswLum_leptonSF_EL_SF_Trigger_UP"             , "weightSyswLum_leptonSF_EL_SF_Trigger_DOWN"             ] ,
    "leptonSF_MU_SF_ID_STAT"             : [ "weightSyswLum_leptonSF_MU_SF_ID_STAT_UP"             , "weightSyswLum_leptonSF_MU_SF_ID_STAT_DOWN"             ] ,
    "leptonSF_MU_SF_ID_SYST"             : [ "weightSyswLum_leptonSF_MU_SF_ID_SYST_UP"             , "weightSyswLum_leptonSF_MU_SF_ID_SYST_DOWN"             ] ,
    "leptonSF_MU_SF_Isol_STAT"           : [ "weightSyswLum_leptonSF_MU_SF_Isol_STAT_UP"           , "weightSyswLum_leptonSF_MU_SF_Isol_STAT_DOWN"           ] ,
    "leptonSF_MU_SF_Isol_SYST"           : [ "weightSyswLum_leptonSF_MU_SF_Isol_SYST_UP"           , "weightSyswLum_leptonSF_MU_SF_Isol_SYST_DOWN"           ] ,
    "leptonSF_MU_SF_Trigger_STAT"        : [ "weightSyswLum_leptonSF_MU_SF_Trigger_STAT_UP"        , "weightSyswLum_leptonSF_MU_SF_Trigger_STAT_DOWN"        ] ,
    "leptonSF_MU_SF_Trigger_SYST"        : [ "weightSyswLum_leptonSF_MU_SF_Trigger_SYST_UP"        , "weightSyswLum_leptonSF_MU_SF_Trigger_SYST_DOWN"        ] ,
    "pileup"                             : [ "weightSyswLum_pileup_UP"                             , "weightSyswLum_pileup_DOWN"                             ] ,
    #"bTagSF77_eigenvars_B_0"             : [ "weightSyswLum_bTagSF_77_eigenvars_B_up_0"            , "weightSyswLum_bTagSF_77_eigenvars_B_down_0"            ] ,
    #"bTagSF77_eigenvars_B_1"             : [ "weightSyswLum_bTagSF_77_eigenvars_B_up_1"            , "weightSyswLum_bTagSF_77_eigenvars_B_down_1"            ] ,
    #"bTagSF77_eigenvars_B_2"             : [ "weightSyswLum_bTagSF_77_eigenvars_B_up_2"            , "weightSyswLum_bTagSF_77_eigenvars_B_down_2"            ] ,
    #"bTagSF77_eigenvars_B_3"             : [ "weightSyswLum_bTagSF_77_eigenvars_B_up_3"            , "weightSyswLum_bTagSF_77_eigenvars_B_down_3"            ] ,
    #"bTagSF77_eigenvars_B_4"             : [ "weightSyswLum_bTagSF_77_eigenvars_B_up_4"            , "weightSyswLum_bTagSF_77_eigenvars_B_down_4"            ] ,
    #"bTagSF77_eigenvars_B_5"             : [ "weightSyswLum_bTagSF_77_eigenvars_B_up_5"            , "weightSyswLum_bTagSF_77_eigenvars_B_down_5"            ] ,
    #"bTagSF77_eigenvars_C_0"             : [ "weightSyswLum_bTagSF_77_eigenvars_C_up_0"            , "weightSyswLum_bTagSF_77_eigenvars_C_down_0"            ] ,
    #"bTagSF77_eigenvars_C_1"             : [ "weightSyswLum_bTagSF_77_eigenvars_C_up_1"            , "weightSyswLum_bTagSF_77_eigenvars_C_down_1"            ] ,
    #"bTagSF77_eigenvars_C_2"             : [ "weightSyswLum_bTagSF_77_eigenvars_C_up_2"            , "weightSyswLum_bTagSF_77_eigenvars_C_down_2"            ] ,
    #"bTagSF77_eigenvars_C_3"             : [ "weightSyswLum_bTagSF_77_eigenvars_C_up_3"            , "weightSyswLum_bTagSF_77_eigenvars_C_down_3"            ] ,
    #"bTagSF77_eigenvars_Light_0"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_0"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_0"        ] ,
    #"bTagSF77_eigenvars_Light_1"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_1"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_1"        ] ,
    #"bTagSF77_eigenvars_Light_2"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_2"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_2"        ] ,
    #"bTagSF77_eigenvars_Light_3"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_3"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_3"        ] ,
    #"bTagSF77_eigenvars_Light_4"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_4"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_4"        ] ,
    #"bTagSF77_eigenvars_Light_5"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_5"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_5"        ] ,
    #"bTagSF77_eigenvars_Light_6"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_6"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_6"        ] ,
    #"bTagSF77_eigenvars_Light_7"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_7"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_7"        ] ,
    #"bTagSF77_eigenvars_Light_8"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_8"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_8"        ] ,
    #"bTagSF77_eigenvars_Light_9"         : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_9"        , "weightSyswLum_bTagSF_77_eigenvars_Light_down_9"        ] ,
    #"bTagSF77_eigenvars_Light_10"        : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_10"       , "weightSyswLum_bTagSF_77_eigenvars_Light_down_10"       ] ,
    #"bTagSF77_eigenvars_Light_11"        : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_11"       , "weightSyswLum_bTagSF_77_eigenvars_Light_down_11"       ] ,
    #"bTagSF77_eigenvars_Light_12"        : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_12"       , "weightSyswLum_bTagSF_77_eigenvars_Light_down_12"       ] ,
    #"bTagSF77_eigenvars_Light_13"        : [ "weightSyswLum_bTagSF_77_eigenvars_Light_up_13"       , "weightSyswLum_bTagSF_77_eigenvars_Light_down_13"       ]
}

configMgr.analysisName   = analysisName
configMgr.outputFileName = "results/result_%s.root" % configMgr.analysisName
configMgr.nomName        = "_nominal"

data_lumi            = '36.1'              # for scaling histograms
nominalWeights       = ['nomWeightwLum']   # for weighting histograms
configMgr.inputLumi  = 1.0                 # all of our ntuples are weighted to 1 fb-1
configMgr.outputLumi = float(data_lumi)
configMgr.setLumiUnits("fb-1")

configMgr.nTOYs            = 5000
configMgr.calculatorType   = 2
configMgr.testStatType     = 3
configMgr.nPoints          = 20
configMgr.doExclusion      = True
configMgr.ReduceCorrMatrix = True
configMgr.writeXML         = True

configMgr.keepSignalRegionType = True ## unsure why we need this.

configMgr.cutsDict = {
    #"llpT_0jet"    : "!SS && elmu && njets==0" ,
    #"llpT_1pjet"   : "!SS && elmu && njets>=1" ,
    #"sllpT_0jet"   : "!SS && elmu && njets==0" ,
    #"sllpT_1pjet"  : "!SS && elmu && njets>=1" ,
    #"lleta_0jet"   : "!SS && elmu && njets==0" ,
    #"lleta_1pjet"  : "!SS && elmu && njets>=1" ,
    #"slleta_0jet"  : "!SS && elmu && njets==0" ,
    #"slleta_1pjet" : "!SS && elmu && njets>=1" ,
    "c_0jet"     : "!SS && elmu && njets==0" ,
    "c_1pjet"    : "!SS && elmu && njets>=1" ,
    #"njets"        : "!SS && elmu"             ,
}

statOnly = False
lumiOnly = False

## setup all of our samples, ttbar, Wt, Zjets, Wjets, WW, Diboson
## store them in a dictionary
aidaSamps = {}
lumSyst = Systematic("LUMI",nominalWeights,1.0374,0.9626,"user","userOverallSys")
for skey, sval in mySamples.iteritems():
    sampleName = str(skey)
    aidaSamps[skey] = Sample(sampleName,sval[1])
    # fakes are in every sample, but with a different tree prefix
    if sampleName is "Fakes":
        aidaSamps[skey].setPrefixTreeName("AIDAfk")
    else:
        aidaSamps[skey].setPrefixTreeName("AIDA")
    aidaSamps[skey].setStatConfig(True)
    ## if the sample is tagged as "1" in the sample dict, give it a mu_XX.
    if sval[0] == 1:
        aidaSamps[skey].setNormFactor("mu_"+sampleName.upper(),1,-10,10)
    ## add the luminosity systematic defined above
    if not statOnly:
        aidaSamps[skey].addSystematic(lumSyst)

    ## set up the nominal weight, give the sample the correct files.
    aidaSamps[skey].setWeights(nominalWeights)
    aidaSamps[skey].setFileList(fileLists[skey])

    ### Loop over tree Systematics, apply them to each sample
    for systK, systV in systKeyListTreesSym.iteritems():
        systype = "overallHistoSys"
        if systV[2]: systype = "overallSys"
        aidaSamps[skey].addSystematic(Systematic(systK,"_nominal",systV[0],systV[1],"tree",systype))

    for systK, systV in systKeyListWeightSysNorm.iteritems():
        systype = "overallSys"
        # don't add these weight systematics to fakes and diboson
        if skey == "Diboson" or skey == "Fakes":
            continue
        aidaSamps[skey].addSystematic(Systematic(systK,nominalWeights,[systV[0]],[systV[1]],"weight",systype))

    for systK in systKeyListTreesAsym:
        systype = 'histoSysOneSideSym'
        aidaSamps[skey].addSystematic(Systematic(systK[0],"_nominal",systK[1],systK[1],"tree",systype))

## set up data.
dataSample = Sample("AIDA_nominal",kBlack)
dataSample.setData()
dataSample.setFileList(fileLists["Data"])

## our main fit configuration
ana = configMgr.addFitConfig("aidaFit")
#ana.errorFillStyle = 3101
c = TCanvas()
leg = ROOT.TLegend(.768,.55,.885,.85)
leg.SetFillStyle(0)
leg.SetLineWidth(0)
leg.SetTextFont(43)
leg.SetTextSize(13)
ent = leg.AddEntry("","Data","p")
ent.SetMarkerColor(kBlack)
ent.SetMarkerStyle(20)
for ss in ["ttbar","Zjets","Wt","WW","Diboson","Fakes"]:
    ent = leg.AddEntry("",ss,"f")
    ent.SetFillColor(mySamples[ss][1])
    ent.SetLineColor(kBlack)
    ent.SetLineWidth(2)
    ent.SetFillStyle(1001)
ent = leg.AddEntry("","Syst","lf")
ent.SetLineColor(kBlack)
ent.SetFillStyle(ana.errorFillStyle)
ent.SetFillColor(ana.errorFillColor)
ent.SetLineWidth(2)

ana.tLegend = leg

## put all samples into a list
anaSampList = [dataSample]
for skey in mySamples:
    anaSampList.append(aidaSamps[skey])
ana.addSamples(anaSampList)

## set up the parameters of interest
POIList = ""
for skey, sval in mySamples.iteritems():
    if sval[0] == 1:
        ana.setSignalSample(aidaSamps[skey])
        if POIList != "":
            POIList += " "
        POIList += "mu_"+skey.upper()

## set up the measurement
meas = ana.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.0374)
meas.addParamSetting("Lumi","const",1.0)
meas.addPOI(POIList)

## form a list of our channels (i.e. regions...)
anaChannelList = []
## chans are really our regions (described above)
for chan in configMgr.cutsDict:
    nbins = 20
    xmin  = 0
    xmax  = 200
    title = "#it{E}_{T}^{miss} [GeV]"
    var = "met"

    if '0j' in chan:
        title = title + " (N_{jets} = 0)"
        xmax = 100
        nbins = 10
    else:
        title = title + " (N_{jets} #geq 1)"

    srchan = ana.addChannel(var,[chan],nbins,xmin,xmax)

    #srchan.variableName = "met"
    srchan.titleY = "Entries/bin"
    #srchan.ATLASLabelX = 0.2
    #srchan.ATLASLabelY = 0.85
    srchan.ATLASLabelText = "Internal"
    srchan.showLumi = True
    srchan.titleX = title

    srchan.useOverflowBin  = True
    srchan.useUnderflowBin = True

    anaChannelList.append(srchan)

## add the channels to the fit
ana.addSignalChannels(anaChannelList)

## all done
